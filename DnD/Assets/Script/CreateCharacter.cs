﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class CreateCharacter : MonoBehaviour {

    public List<GameObject> charList;
    public List<Image> imageList;
    public GameObject newChar;

    public Text cName;
    public List<Text> stats;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CharCreate()
    {
        GameObject createdChar;

        createdChar = Instantiate(newChar, this.transform.position, this.transform.rotation) as GameObject;
        charList.Add(createdChar);
        createdChar.GetComponent<Character>().name = cName.text;
        createdChar.GetComponent<Character>().str = int.Parse(stats[0].text);
        createdChar.GetComponent<Character>().dex = int.Parse(stats[1].text);
        createdChar.GetComponent<Character>().con = int.Parse(stats[2].text);
        createdChar.GetComponent<Character>().intel = int.Parse(stats[3].text);
        createdChar.GetComponent<Character>().wis = int.Parse(stats[4].text);
        createdChar.GetComponent<Character>().charisma = int.Parse(stats[5].text);
    }
}
